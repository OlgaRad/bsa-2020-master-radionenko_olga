const assert = require('assert');
const {URL} = require('url');

describe('Your Playground test suite', () => {
    
    
    it('Wait for element to be enabled ', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "expected_conditions.html"]');
        waitCondition.click();
        const buttonTrigger = $('button#visibility_trigger');
        buttonTrigger.click();
        const buttonClickMe = $('button#visibility_target');
        buttonClickMe.waitForDisplayed(3000);
        buttonClickMe.click();
    });

    it('Wait for text/value to have specific values', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "expected_conditions.html"]');
        waitCondition.click();
        const buttonTrigger = $('button#text_value_trigger');
        buttonTrigger.click();
        const inputText = $('input#wait_for_value');
        inputText.waitForDisplayed(4000);
        const getText = inputText.getText();
        console.log(getText);
    });

    it('Keyboard Actions', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "keyboard_events.html"]');
        waitCondition.click();
        const textArea = $('textarea#area');
        textArea.setValue('W');
        const key = $('span#key');
        key.waitForDisplayed(1000);
        const keyText = key.getText();
        console.log(keyText);
        const keyCode = $('span#code');
        keyCode.waitForDisplayed(1000);
        const keyTextCode = keyCode.getText();
        console.log(keyTextCode);
    });

    it('Mouse Click Actions (implicit and explicit position)', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "mouse_events.html"]');
        waitCondition.click();
        const clickArea = $('div#click_area');
        clickArea.click();
        const clickX = $('label#click_x');
        const textX = clickX.getText();
        console.log(textX);
        const clickY = $('label#click_y');
        const textY = clickY.getText();
        console.log(textY);
    });

    it('Mouse Hover', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "mouse_events.html"]');
        waitCondition.click();
        const hoverButton = $('button.dropbtn');
        hoverButton.moveTo();
     
    });

    it('Drag and Drop)', () =>{
        browser.url('https://dineshvelhal.github.io/testautomation-playground/');
        const waitCondition = $('a[href = "mouse_events.html"]');
        waitCondition.click();
        const dragSource = $('button#drag_source');
        const dropTarget = $('div#drop_target');
        dragSource.dragAndDrop(dropTarget);
    });
});


