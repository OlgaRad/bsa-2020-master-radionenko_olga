class SettingsPage {

    get firstNameInput() {
        return $('input[name = "firstName"]')        
    };
    get lastNameInput() {
        return $('input[name ="lastName"]')
    };
    get emailInput() {
        return $('input[name="email"]')
    };
    get newPasswordInput() {
        return $('input[placeholder="New password"]')
    };
    get createButton() {
        return $('//button[contains(., "Create")]')
    };
    get emailLoginInput() {
        return $('input[placeholder = "Your Email"]')
    };
    get passwordInput() {
        return $('input[placeholder = "New password"]')
    };
    get loginButton() {
        return $('//button[contains(., "Login")]')
    };
    get lists() {
        return $('a[href = "/my-lists"]')
    };

}

module.exports = SettingsPage;
