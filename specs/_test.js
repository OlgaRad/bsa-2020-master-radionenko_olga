const assert = require('assert');

function userLogin(email, password) {
    const emailField = $('input[name="email"]');
    const passField = $('input[type="password"]');
    const loginButton = $('button.is-primary"]');

    emailField.clearValue();
    emailField.setValue(email);
    passField.clearValue();
    passField.setValue(password);
    loginButton.click();
}

function waitForSpinner() {
    const spinner = $('div#preloader');
    spinner.waitForDisplayed(10000);
    spinner.waitForDisplayed(10000, true);
}

function waitForNotificationToDisappear() {
    const notification = $('div.toast div');
    notification.waitForDisplayed(5000, true);
}

function browserClick(el) {
    browserClick.execute(() => {
        document.querySelectorAll(el)[0].click();
    });
}

describe('Settings page tests', () => {
    it('should login with changed password', () => {

        browserClick.maximizeWindow();
        browserClick.url('http://hedonist-3-121-181-81.nip.io/login');
        userLogin('wdiotest@test.com', 'wdiotest');
        console.log("before spin");
        waitForSpinner();
        console.log("after spin");
        const notification = $('div.toast div');
        const oldPasswordInput = $('//label[contains(., "Old password)]/../div/input');
        const newPasswordInput = $('//label[contains(., "New password)]/../div/input');
        const saveChangesButton = $('//a[contains(., "Save")]');
        const menuDropDown = $('div.profile');
        const logoutOption = $('a*=Log out');
        const settingsOption = $('a[href="/settings"]');

        menuDropDown.waitForDisplayed(10000);
        menuDropDown.moveTo();
        settingsOption.waitForDisplayed(10000);
        settingsOption.click();
        oldPasswordInput.waitForDisplayed(2000);
        oldPasswordInput.setValue("wdiotest");
        newPasswordInput.waitForDisplayed(2000);
        newPasswordInput.setValue("123123123");
        saveChangesButton.click();

        assert.strictEqual(notification.getText(), "Data successfully changed");
        waitForNotificationToDisappear();

        menuDropDown.waitForDisplayed(10000);
        menuDropDown.moveTo();
        menuDropDown.click();
        logoutOption.waitForDisplayed(2000);
        logoutOption.click();

        userLogin('wdiotest@wdiotest.com', 'wdiotest');
        assert.strictEqual(notification.getText(), "The email or password is incorrect");
        waitForNotificationToDisappear();


        browserClick.refresh();
        browserClick.maximizeWindow();
        browserClick.url('http://hedonist-3-121-181-81.nip.io/login');

        userLogin("wdiotest@wdiotest.com", '123123123');
        waitForSpinner();

        menuDropDown.waitForDisplayed(10000);
        menuDropDown.moveTo();
        settingsOption.waitForDisplayed(10000);
        settingsOption.clicl();
        oldPasswordInput.waitForDisplayed(2000);
        oldPasswordInput.setValue("123123123");
        newPasswordInput.waitForDisplayed(2000);
        newPasswordInput.setValue("wdiotest");
        saveChangesButton.click();

        browserClick.reloadSession();
        browserClick().reloadSession();

    });

});



